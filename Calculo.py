
import math


class CoordeanadasPD:
    def __init__(self, longitud, latitud, altura, a, aplanamiento):
        self.longitud = longitud
        self.latitud = latitud
        self.altura = altura
        self.a=a
        self.aplanamiento=aplanamiento

    def CalculoN(self):
        l=self.longitud
        j=self.altura
        k=self.latitud
        a=self.a
        f=self.aplanamiento
       
        b=(float(a)/float(a)*float(f))
       
        e=float((math.pow(float(a), 2)-math.pow(float(b),2))/math.pow(float(a),2))
       
        eprima=float((math.pow(float(a), 2)-math.pow(float(b),2))/math.pow(float(b),2))
       
        # N=float(a/(1-(e**2)*(math.sin(longitud)**2))**0.5)
       
        N = float(float(self.a)/math.pow(1-e*(math.pow(math.sin(float(l)),2)),0.5))
        Z = float(N*(((1-e)+float(j))*math.sin(float(l))))
        print("Coordenada Z:")
        print(Z)
        X=float((N+float(j))*math.cos(float(l))*math.cos(float(k)))
        print("Coordenada X:")
        print(X)
        Y=float((N+float(j))*math.cos(float(l))*math.sin(float(k)))
        print("Coordenada Y:")
        print(Y)
        print("Calculo de N:")
        return N,eprima

        



         